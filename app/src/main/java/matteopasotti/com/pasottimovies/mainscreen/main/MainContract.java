package matteopasotti.com.pasottimovies.mainscreen.main;

import java.text.ParseException;
import java.util.List;

import matteopasotti.com.pasottimovies.pojo.Movie;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public interface MainContract {

    interface View {

        void initMovieList(List<Movie> movies);

        void updateMovieList(List<Movie> movies);


        void filterMovieList(List<Movie> filteredMovies);

        boolean isFiltered();


        void showError(String message);

        void showToast(String message);

        void showFilter();
    }

    interface Presenter {

        void getMostPopularMovies(int page);

        void getMostPopularMoviesByYear(int page, int year);

        void editMovieToFav(Movie movie, int position);

        void saveMoviesOnDb(List<Movie> movies);

        void getMoviesFromDB();

        boolean isNetworkAvailable();

        void resetFilter();
    }
}
