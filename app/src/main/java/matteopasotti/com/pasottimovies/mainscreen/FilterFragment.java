package matteopasotti.com.pasottimovies.mainscreen;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import matteopasotti.com.pasottimovies.R;

/**
 * Created by matteo.pasotti on 09/10/2017.
 */

public class FilterFragment extends Fragment {

    @BindView(R.id.spinner_filter)
    Spinner spinner;

    @BindView(R.id.filter_text_layout)
    LinearLayout filterTextLayout;

    @BindView(R.id.title_filter)
    TextView titleFilter;

    @BindView(R.id.filter)
    EditText filter;

    @BindView(R.id.hideFragment)
    ImageView hide;

    @BindView(R.id.buttonFilter)
    Button buttonFilter;

    @BindView(R.id.buttonReset)
    Button buttonReset;

    //filter voices
    private List<String> filters;

    private ArrayAdapter<String> filtersAdapter;

    private onFilterSelected onFilterSelected;

    private Activity activity;

    public FilterFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        filters = new ArrayList<>();
        filters.add("");
        filters.add("Year");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_filter, container, false);
        ButterKnife.bind(this, viewGroup);

        filtersAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, filters);
        filtersAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(filtersAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String itemSelected = parent.getItemAtPosition(position).toString();

                if (!itemSelected.equals("")) {
                    filterTextLayout.setVisibility(View.VISIBLE);
                    titleFilter.setText("Select " + itemSelected);

                    if (itemSelected.equals("Year")) {
                        filter.setInputType(InputType.TYPE_CLASS_NUMBER);
                        int maxLength = 4;
                        filter.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return viewGroup;
    }

    @OnClick(R.id.hideFragment)
    public void hideFilterFragment() {
        try {
            ((onFilterSelected) activity).onResetFilter();
            getActivity().getFragmentManager().popBackStack();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.buttonFilter)
    public void applyFilter() {
        String textFilter;
        String typeFilter;

        if ((spinner.getSelectedItem() != null && !spinner.getSelectedItem().toString().equals("")) && filter.getText().toString() != null) {
            typeFilter = spinner.getSelectedItem().toString();
            textFilter = filter.getText().toString();

            onFilterSelected.onSelectFilter(typeFilter, textFilter);

        } else {
            Toast.makeText(getActivity(), "Complete the fields!", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.buttonReset)
    public void reset() {
        spinner.setAdapter(filtersAdapter);
        filter.setText("");
        filterTextLayout.setVisibility(View.GONE);
        onFilterSelected.onResetFilter();
    }


    public interface onFilterSelected {

        void onSelectFilter(String typeFilter, String filter);

        void onResetFilter();
    }

    public void setOnFilterSelected(onFilterSelected onFilterSelected) {
        this.onFilterSelected = onFilterSelected;
    }
}
