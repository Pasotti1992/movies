package matteopasotti.com.pasottimovies.mainscreen.main;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ProcessModelTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import matteopasotti.com.pasottimovies.api.MoviesApiInterface;
import matteopasotti.com.pasottimovies.common.Constants;
import matteopasotti.com.pasottimovies.common.MyDatabase;
import matteopasotti.com.pasottimovies.pojo.Movie;
import matteopasotti.com.pasottimovies.pojo.MovieResponse;
import matteopasotti.com.pasottimovies.pojo.Movie_Table;
import matteopasotti.com.pasottimovies.util.shared_pref.LocalStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public class MainPresenter implements MainContract.Presenter {

    Retrofit retrofit;

    Application application;

    LocalStorage localStorage;

    MainContract.View mView;

    private List<Movie> movies = new ArrayList<>();
    private List<Movie> filteredMovies = new ArrayList<>();

    @Inject
    public MainPresenter(Retrofit retrofit, Application app, LocalStorage localStorage, MainContract.View view) {
        this.retrofit = retrofit;
        this.application = app;
        this.localStorage = localStorage;
        this.mView = view;
    }


    @Override
    public void getMostPopularMovies(int page) {

        if (isNetworkAvailable()) {
            retrofit.create(MoviesApiInterface.class).getMostPopularMovies(Constants.API_KEY, "popularity.desc", String.valueOf(page)).enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    List<Movie> moviesResp = response.body() != null ? response.body().getResults() : null;
                    Log.d("MainPresenter", "getMostPopularMovies SUCCESS");

                    if (moviesResp != null && moviesResp.size() > 0) {
                        for (Movie m : moviesResp) {
                            m.setSaved(localStorage.isMovieFav(m));
                            movies.add(m);
                        }

                        if (response.body().getPage() > 1) {
                            mView.updateMovieList(movies);
                        } else {
                            mView.initMovieList(movies);
                        }
                    }
                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    Log.d("MainPresenter", "getMostPopularMovies FAILED");
                }
            });
        } else {
            getMoviesFromDB();
        }

    }

    @Override
    public void getMostPopularMoviesByYear(int page, int year) {
        if (isNetworkAvailable()) {
            Log.d("MainPresenter", "getMostPopularMoviesByYear filter by " + year);
            retrofit.create(MoviesApiInterface.class).getMostPopularMoviesByYear(Constants.API_KEY, "popularity.desc", String.valueOf(page), year).enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    List<Movie> moviesResp = response.body() != null ? response.body().getResults() : null;
                    Log.d("MainPresenter", "getMostPopularMoviesByYear SUCCESS");

                    if (moviesResp != null && moviesResp.size() > 0) {
                        for (Movie m : moviesResp) {
                            m.setSaved(localStorage.isMovieFav(m));
                            filteredMovies.add(m);
                        }

                        if(response.body().getPage() > 1) {
                            mView.updateMovieList(filteredMovies);
                        }else {
                            mView.filterMovieList(filteredMovies);
                        }

                    }
                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    Log.d("MainPresenter", "getMostPopularMoviesByYear FAILED");
                }
            });
        }
    }

    @Override
    public void editMovieToFav(Movie movie, int position) {
        if (movie.isSaved()) {
            //it was on the preference
            //i remove this movie from fav movies on preferences
            movie.setSaved(false);
            localStorage.removeMovieFromFav(movie);
        } else {
            //add this movie to fav movies on preferences
            movie.setSaved(true);
            localStorage.addMovieToFav(movie);
        }

        if (!mView.isFiltered()) {
            movies.set(position, movie);
            mView.updateMovieList(movies);
        } else {
            filteredMovies.set(position, movie);
            mView.updateMovieList(filteredMovies);
        }

    }

    @Override
    public void saveMoviesOnDb(List<Movie> movies) {
        //write movies on db thus they'll be available offline
        if (movies != null && movies.size() > 0) {
            FlowManager.getDatabase(MyDatabase.class)
                    .beginTransactionAsync(new ProcessModelTransaction.Builder<>(
                            new ProcessModelTransaction.ProcessModel<Movie>() {
                                @Override
                                public void processModel(Movie movie) {
                                    // do work here -- i.e. user.delete() or user.update()
                                    movie.save();
                                }
                            }).addAll(movies).build())
                    .error(new Transaction.Error() {
                        @Override
                        public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {
                            Log.d("MainPresenter", "saveMoviesOnDb FAILED");
                            mView.showError(error.getMessage());
                        }
                    })
                    .success(new Transaction.Success() {
                        @Override
                        public void onSuccess(@NonNull Transaction transaction) {
                            Log.d("MainPresenter", "saveMoviesOnDb SUCCESS");
                            mView.showToast("Movies saved!");
                        }
                    }).build().execute();
        }
    }

    @Override
    public void getMoviesFromDB() {
        //take offline movies
        SQLite.select()
                .from(Movie.class)
                .orderBy(Movie_Table.popularity, false)
                .async()
                .queryListResultCallback(new QueryTransaction.QueryResultListCallback<Movie>() {
                    @Override
                    public void onListQueryResult(QueryTransaction transaction, @NonNull List<Movie> tResult) {
                        if (tResult != null && tResult.size() > 0) {
                            mView.initMovieList(tResult);
                        } else {
                            mView.showToast("Connection problem and you haven't offline movies");
                        }

                    }
                }).execute();

    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) application.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    @Override
    public void resetFilter() {
        filteredMovies = new ArrayList<>();
    }

}
