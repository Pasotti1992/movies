package matteopasotti.com.pasottimovies.mainscreen.main;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.pasottimovies.R;
import matteopasotti.com.pasottimovies.common.MyApp;
import matteopasotti.com.pasottimovies.data.component.DaggerMainComponent;
import matteopasotti.com.pasottimovies.data.module.main.MainModule;
import matteopasotti.com.pasottimovies.mainscreen.FilterFragment;
import matteopasotti.com.pasottimovies.pojo.Movie;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public class MainActivity extends AppCompatActivity implements MainContract.View, MovieAdapter.onFavouriteChange, FilterFragment.onFilterSelected {

    @BindView(R.id.moviesRv)
    RecyclerView rvMovies;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.container_filterfragment)
    FrameLayout containerFilterFragment;

    @Inject
    MainPresenter mPresenter;

    private MovieAdapter adapter;
    private List<Movie> movies, filteredMovies;
    private ProgressDialog progressDialog;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    int page = 1;
    private FilterFragment filterFragment;
    private boolean isFiltered = false;
    private String filterType, filterText = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DaggerMainComponent.builder()
                .netComponent(((MyApp) getApplicationContext()).getNetComponent())
                .mainModule(new MainModule(this))
                .build().inject(this);

        initView();

        mPresenter.getMostPopularMovies(page);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.action_offline ) {
            mPresenter.saveMoviesOnDb(movies);
        } else if ( id == R.id.action_filter ) {
            showFilter();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {

        setSupportActionBar(toolbar);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL , false);
        rvMovies.setLayoutManager(layoutManager);
        rvMovies.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //only when scrolling up
                if(mPresenter.isNetworkAvailable()) {
                    if( dy > 0 ) {

                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                        if( loading ) {
                            if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                            {
                                page++;

                                if(isFiltered){
                                    if(filterType.equals("Year")){
                                        mPresenter.getMostPopularMoviesByYear(page, Integer.valueOf(filterText));
                                    }

                                } else {
                                    mPresenter.getMostPopularMovies(page);
                                }
                            }
                        }
                    }
                } else {
                    showError("Check your internet connection");
                }

            }
        });
    }

    @Override
    public void initMovieList(List<Movie> newMovies) {

        movies = newMovies;
        adapter = new MovieAdapter(MainActivity.this, movies);
        rvMovies.setAdapter(adapter);
        adapter.setOnFavouriteChange(this);

    }

    @Override
    public void updateMovieList(List<Movie> favMovies) {
        if(!isFiltered) {
            movies = favMovies;
        } else {
            filteredMovies = favMovies;
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void filterMovieList(List<Movie> filteredMovies) {
        isFiltered = true;
        this.filteredMovies = filteredMovies;
        adapter = new MovieAdapter(MainActivity.this, this.filteredMovies);
        rvMovies.setAdapter(adapter);
        adapter.setOnFavouriteChange(this);
    }

    @Override
    public boolean isFiltered() {
        return isFiltered;
    }

    @Override
    public void showError(String message) {
        //show error message toast
        Toast.makeText(getApplicationContext(), "Error " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFilter() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        filterFragment = new FilterFragment();
        filterFragment.setOnFilterSelected(this);
        ft.add(R.id.container_filterfragment, filterFragment, "FilterFragment");
        ft.addToBackStack("FilterFragment");
        ft.commit();
    }


    @Override
    public void onChange(Movie movie, int position) {
        //i'll add or remove a movie from preferences
        mPresenter.editMovieToFav(movie,position);
    }

    @Override
    public void onSelectFilter(String typeFilter, String filter) {
        page = 1;
        filterType = typeFilter;
        filterText = filter;
        mPresenter.getMostPopularMoviesByYear(page, Integer.valueOf(filter));
    }

    @Override
    public void onResetFilter() {
        mPresenter.resetFilter();
        isFiltered = false;
        filterType = null;
        filterText = null;
        adapter = new MovieAdapter(MainActivity.this, movies);
        rvMovies.setAdapter(adapter);
        adapter.setOnFavouriteChange(this);
    }
}
