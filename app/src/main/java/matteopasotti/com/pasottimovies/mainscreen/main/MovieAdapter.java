package matteopasotti.com.pasottimovies.mainscreen.main;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.pasottimovies.R;
import matteopasotti.com.pasottimovies.pojo.Movie;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private List<Movie> movies;
    private Context context;
    private onFavouriteChange onFavouriteChange;

    public MovieAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.movie_row, parent, false);
        MovieViewHolder holder = new MovieViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MovieViewHolder holder, final int position) {

        final Movie movie = movies.get(position);

        holder.movieTitle.setText(movie.getTitle());
        holder.releaseDate.setText(movie.getReleaseDate());
        holder.voteAverage.setText("vote: " + String.valueOf(movie.getVote_average()));
        holder.popularity.setText("popularity: " + String.valueOf(movie.getPopularity()));

        Uri imageUri = Uri.parse("https://image.tmdb.org/t/p/w185" + movie.getPosterPath());
        Glide.with(context).load(imageUri)
                .thumbnail(0.5f)
                .crossFade()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.movieImage);

        holder.favButton.setChecked(movie.isSaved());
        if (holder.favButton.isChecked()) {
            holder.favButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.star_filled));
        } else {
            holder.favButton.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.star_unfilled));
        }
        holder.favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFavouriteChange.onChange(movie, position);
            }
        });

    }

    public void setOnFavouriteChange(onFavouriteChange onFavouriteChange) {
        this.onFavouriteChange = onFavouriteChange;
    }

    interface onFavouriteChange {
        void onChange(Movie movie, int position);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movieImage)
        ImageView movieImage;

        @BindView(R.id.movieTitle)
        TextView movieTitle;

        @BindView(R.id.releaseDate)
        TextView releaseDate;

        @BindView(R.id.voteAverage)
        TextView voteAverage;

        @BindView(R.id.popularity)
        TextView popularity;

        @BindView(R.id.favButton)
        ToggleButton favButton;

        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }

}
