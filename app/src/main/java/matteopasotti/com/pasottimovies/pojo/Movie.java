package matteopasotti.com.pasottimovies.pojo;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import matteopasotti.com.pasottimovies.common.MyDatabase;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

@Table(database = MyDatabase.class)
public class Movie extends BaseModel {

    @SerializedName("id")
    @Column
    @PrimaryKey
    private Integer id;

    @SerializedName("poster_path")
    @Column
    private String posterPath;

    @SerializedName("release_date")
    @Column
    private String releaseDate;

    @SerializedName("title")
    @Column
    private String title;

    @SerializedName("genre_ids")
    private List<Integer> genreIds = new ArrayList<Integer>();

    @SerializedName("popularity")
    @Column
    private Double popularity;

    @SerializedName("vote_average")
    @Column
    private Double vote_average;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @SerializedName("year")
    @Column
    private Integer year;

    private boolean isSaved = false;

    public Movie() {}

    public Movie(Integer id, String title , String releaseDate, List<Integer> genreIds, String posterPath, Double popularity, Integer year) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.genreIds = genreIds;
        this.posterPath = posterPath;
        this.popularity = popularity;
        this.year = year;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public Double getVote_average() {
        return vote_average;
    }

    public void setVote_average(Double vote_average) {
        this.vote_average = vote_average;
    }

    public String getYearFromDate(String date)  {
        String year = null;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            year = calendar.get(Calendar.YEAR) <= 9 ? "0" + calendar.get(Calendar.YEAR) : String.valueOf(calendar.get(Calendar.YEAR));
        }catch (Exception e) {
            e.printStackTrace();
        }


        return year;
    }
}
