package matteopasotti.com.pasottimovies.util.shared_pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import matteopasotti.com.pasottimovies.pojo.Movie;
import matteopasotti.com.pasottimovies.util.shared_pref.LocalStorage;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public class SharedPrefStorage implements LocalStorage {

    private Context context;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public SharedPrefStorage(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        this.editor = preferences.edit();
    }


    @Override
    public void addMovieToFav(Movie movie) {

        Gson gson = new Gson();
        List<String> favMovies = getFavMovies();

        if(favMovies != null && favMovies.size() > 0) {
            favMovies.add(String.valueOf(movie.getId()));
        } else {
            favMovies = new ArrayList<>();
            favMovies.add(String.valueOf(movie.getId()));
        }

        String json = gson.toJson(favMovies);
        editor.putString("favMovies", json);
        editor.commit();
    }

    @Override
    public void removeMovieFromFav(Movie movie) {
        Gson gson = new Gson();
        List<String> favMovies = getFavMovies();

        if(favMovies != null && favMovies.size() > 0) {
            favMovies.remove(String.valueOf(movie.getId()));
        }

        String json = gson.toJson(favMovies);
        editor.putString("favMovies", json);
        editor.commit();
    }

    @Override
    public List<String> getFavMovies() {

        Gson gson = new Gson();
        String json = preferences.getString("favMovies", null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> arrayList = gson.fromJson(json, type);

        return arrayList;
    }

    @Override
    public boolean isMovieFav(Movie movie) {
        List<String> favMovies = getFavMovies();

        if(favMovies != null && favMovies.size() > 0) {
            return favMovies.contains(String.valueOf(movie.getId()));
        }
        return false;
    }
}
