package matteopasotti.com.pasottimovies.util.shared_pref;

import java.util.List;

import matteopasotti.com.pasottimovies.pojo.Movie;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public interface LocalStorage {

    void addMovieToFav(Movie movie);

    void removeMovieFromFav(Movie movie);

    List<String> getFavMovies();

    boolean isMovieFav(Movie movie);
}
