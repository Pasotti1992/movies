package matteopasotti.com.pasottimovies.common;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import matteopasotti.com.pasottimovies.data.component.DaggerNetComponent;
import matteopasotti.com.pasottimovies.data.component.NetComponent;
import matteopasotti.com.pasottimovies.data.module.AppModule;
import matteopasotti.com.pasottimovies.data.module.NetModule;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public class MyApp extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // This instantiates DBFlow
        FlowManager.init(new FlowConfig.Builder(this).build());

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();
    }

    public NetComponent getNetComponent() { return netComponent; }
}
