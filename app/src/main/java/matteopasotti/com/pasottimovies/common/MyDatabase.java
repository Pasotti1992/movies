package matteopasotti.com.pasottimovies.common;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by matteo.pasotti on 05/10/2017.
 */

@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {

    public static final String NAME = "MoviesDatabase";

    public static final int VERSION = 3;
}
