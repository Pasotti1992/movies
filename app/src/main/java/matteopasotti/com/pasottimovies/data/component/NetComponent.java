package matteopasotti.com.pasottimovies.data.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import matteopasotti.com.pasottimovies.data.module.AppModule;
import matteopasotti.com.pasottimovies.data.module.NetModule;
import matteopasotti.com.pasottimovies.util.shared_pref.LocalStorage;
import retrofit2.Retrofit;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    Retrofit retrofit();
    Application provideApplication();
    LocalStorage provideLocalStorage();
}
