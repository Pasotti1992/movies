package matteopasotti.com.pasottimovies.data.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import matteopasotti.com.pasottimovies.util.shared_pref.LocalStorage;
import matteopasotti.com.pasottimovies.util.shared_pref.SharedPrefStorage;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

@Module
public class AppModule {

    Application application;

    public AppModule(Application mApplication) {
        this.application = mApplication;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    LocalStorage provideLocalStorage() {
        return new SharedPrefStorage(application);
    }
}
