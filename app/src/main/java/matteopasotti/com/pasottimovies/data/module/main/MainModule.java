package matteopasotti.com.pasottimovies.data.module.main;

import dagger.Module;
import dagger.Provides;
import matteopasotti.com.pasottimovies.mainscreen.main.MainContract;
import matteopasotti.com.pasottimovies.util.scopes.MainScope;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

@Module
public class MainModule {

    private final MainContract.View view;

    public MainModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    @MainScope
    MainContract.View provideMainView() {return view; }
}
