package matteopasotti.com.pasottimovies.data.component;

import dagger.Component;
import matteopasotti.com.pasottimovies.data.module.main.MainModule;
import matteopasotti.com.pasottimovies.mainscreen.main.MainActivity;
import matteopasotti.com.pasottimovies.util.scopes.MainScope;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

@Component(dependencies = NetComponent.class, modules = MainModule.class)
@MainScope
public interface MainComponent {

    void inject(MainActivity mainActivity);
}
