package matteopasotti.com.pasottimovies.api;

import matteopasotti.com.pasottimovies.pojo.MovieResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by matteo.pasotti on 04/10/2017.
 */

public interface MoviesApiInterface {

    ///discover/movie?sort_by=popularity.desc
    @GET("discover/movie")
    Call<MovieResponse> getMostPopularMovies(@Query("api_key") String apiKey, @Query("sort_by") String sortCasual, @Query("page") String page);

    @GET("discover/movie")
    Call<MovieResponse> getMostPopularMoviesByYear(@Query("api_key") String apiKey, @Query("sort_by") String sortCasual, @Query("page") String page, @Query("year") int year);
}
